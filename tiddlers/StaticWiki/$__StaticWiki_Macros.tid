caption: Macros
created: 20181210141551812
git_repo: framagit.org/know-rmandie/WikiFoncier
modified: 20190123113338796
tags: $:/tags/Macro $:/StaticWiki
title: $:/StaticWiki/Macros
type: text/vnd.tiddlywiki

\define makeGitUrl() https://$(gitRoot)$/blob/master/tiddlers/$(gitLink)$
\define innerMakeGitLink(linkText)
<$set name="gitRoot" value={{!!git_repo}}>
<$set name="gitLink" value={{$:/config/OriginalTiddlerPaths##$(thisTiddler)$}}>
  <a href=<<makeGitUrl>> class="tc-tiddlylink-external" target="_blank" rel="noopener noreferrer">$linkText$</a>
</$set></$set>
\end
\define outerDraftMakeGitLink(linkText)
<$set name="thisTiddler" value={{$(currentTiddler)$!!draft.of}}>
<<innerMakeGitLink "$linkText$">>
</$set>
\end
\define outerMakeGitLink(linkText)
<$set name="thisTiddler" value="$(currentTiddler)$">
<<innerMakeGitLink "$linkText$">>
</$set>
\end
\define makeIssueUrl() https://$(gitRoot)$/issues/new?issue[title]=$(titre)$&issue[description]=$(description)$%20($(gitLink)$)
\define innerMakeIssueLink(linkText linkClass)
<$set name="gitRoot" value={{$:/StaticWiki/Macros!!git_repo}}>
<$set name="titre" value={{$:/temp/issue!!titre}}>
<$set name="description" value={{$:/temp/issue!!description}}>
<$set name="gitLink" value={{$:/config/OriginalTiddlerPaths##$(thisTiddler)$}}>
  <a href=<<makeIssueUrl>> class=$linkClass$ target="_blank" rel="noopener noreferrer">$linkText$</a>
</$set></$set></$set>
\end
\define outerDraftMakeIssueLink(linkText)
<$set name="gitRoot" value={{$:/StaticWiki/Macros!!git_repo}}>
<$set name="titre" value="intitulé de votre commentaire, proposition, ...">
<$set name="description" value={{!!title}}>
  <a href=<<makeIssueUrl>> class=$linkClass$ target="_blank" rel="noopener noreferrer">$linkText$</a>
</$set></$set></$set>
\end
\define captitle() <$view field="caption">{{!!title}}</$view>
\define caplink() <$link to={{!!title}}><$view field="caption">{{!!title}}</$view></$link>

Les macros TiddlyWiki définies dans ce wiki pour le faire tourner, ainsi que quelques variables associées :

!!! Variables

* `git_repo` : <$edit-text field="git_repo" class="simple long"/>

!!! Macros

* `makeGitUrl()` fourni une url vers l'original de la page courante dans le dépôt gitlab : `https://$(gitRoot)$/blob/master/tiddlers/$(gitLink)$`, où on pourra laisser un commentaire ou proposer des modifications. A besoin que les variables `$(gitRoot)$` et `$(gitLink)$` aient été définies
* `outerMakeGitLink(linkText)` (via `innerMakeGitLink(linkText)`) crée un lien en utilisant `makeGitLink` et le texte //linkText//
* `outerDraftMakeGitLink(linkText)` produit un lien en utilisant `makeGitLink` et le texte //linkText//
* `makeIssueUrl` crée une url vers le gitlab défini dans le champ `git_repo` pour poster une issue avec `$(titre)$` et `$(description)$`
* `innerMakeIssueLink(linkText linkClass)` crée un lien vers cette url avec le texte //linkText// et la classe //linkClass//
* `outerDraftMakeIssueLink(linkText)` crée un lien vers une nouvelle issue à propos du tiddler courant
* `captitle()` fourni le titre "étendu" de la page (champ `caption`) ou à défaut le titre standard (moins joli et lisible)
* `caplink()` fourni un lien vers un article utilisant le titre "étendu" de la page (champ `caption`)
* $:/StaticWiki/Macros/toc-spectre contient les macros permettant de faire fonctionner le menu spectre dépliant
