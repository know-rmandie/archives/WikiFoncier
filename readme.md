_Le [dépôt principal][repo] est hébergé sur l'instance gitlab de [Framasoft][framasoft]. Il est toutefois possible de trouver des copies hébergées sur gitlab ou github. Dans tous les cas, merci de faire vos remarques, commentaires et rapports de bugs [sur framagit][issues]_

# WikiFoncier
est une expérience menée dans le cadre de la réalisation de l'[étude pour l'optimisation du foncier d'activité][etude_optimisation_foncier_activite]. Partant du constat que la description des outils et méthodes foncières s'accomodait mal d'une présentation linéaire et nécessitait des mises à jour régulières, nous avons imaginé utiliser le principe du [wiki][wiki] pour les présenter.

Vous pouvez bien sûr explorer [le Wiki en ligne][fio-pages] ou encore [télécharger le wiki autonome][downloadWiki] pour un usage déconnecté.

## Accès, usages et commentaires ouverts
Passé cet usage qui nous l'espérons sera utile à vos projets ou à ceux que vous accompagnez, vous êtes également invités à l'action pour l'amélioration en continu de WikiFoncier.

Cela peut passer par de simples commentaires et rapports de bugs à travers l'outil intégré de la présente plate-forme, par la rédaction de propositions de fiches à intégrer au wiki ou pourquoi par la contribution au code de l'outil lui-même. Plus de détails sont fournis dans [le guide de contribution](contributing.md) ainsi que dans la documentation du Wiki.

## Sources / licenses
* Le moteur de wiki [TiddlyWiki][tiddlywiki] bien sûr - [License BSD 3-Clause](https://github.com/Jermolene/TiddlyWiki5/blob/master/license.md)
* La plate-forme CSS [spectre][spectre] - Licence MIT

Tous les développements et bricolages effectués sur ces bases et les contenus sont quant à eux diffusés sous [licence ouverte](licence.md)

## Parcours
* **[framagit][repo]** > [frama.io pages][fio-pages]


[repo]: https://framagit.org/know-rmandie/WikiFoncier
[issues]: https://framagit.org/know-rmandie/WikiFoncier/issues

[etude_optimisation_foncier_activite]: http://normandie.developpement-durable.gouv.fr/etude-pour-l-optimisation-du-foncier-des-espaces-d-a2049.html
[wiki]: https://fr.wikipedia.org/wiki/Wiki
[fio-pages]: http://know-rmandie.frama.io/WikiFoncier
[downloadWiki]: http://know-rmandie.frama.io/WikiFoncier/telecharger.html

[framasoft]: http://framasoft.org
[tiddlywiki]: http://tiddlywiki.com
[spectre]: https://picturepan2.github.io/spectre/index.html
